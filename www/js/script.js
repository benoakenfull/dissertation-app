// (function($) {
        
//     var App = {
//         init: function( options ) {

//             this.type = 'POST';
//             this.dataType = 'json';
//             this.settings = $.extend({}, this.defaults, options);

//             this.cache();
//             // this.ajaxCall();
//             this.bindEvents();
//             return this;
//         },

//         cache: function() { 
//             this.button = this.settings.button;
//             this.dataSource = this.settings.dataSource;
//         },
        
//         defaults: {
//             'url': '',
//             'container': $('ul.tweets'),
//             button: $('#login')
//         },

//         bindEvents: function(){

//             this.button.on( 'click', this.ajaxAppCall )
//         },

//         ajaxAppCall: function() {
//             var self = App;

//             $.ajax({
//                 type: self.type,
//                 url: 'http://192.168.1.68/bocoapp/' + self.settings.url,
//                 dataType: self.dataType,
//                 data: self.settings.ajaxData,
//                 success: function(data){
//                     if(data.error === false){
//                         window.location.href = self.settings.success
//                     }else if(data.error === true){
//                         alert('error');
//                     }       
//                 }
//             })
//         },

//     };

//     window.App = App.init();



// })(jQuery);


/* ---------------------------------------------
* Returns url value
* --------------------------------------------- */
function getQueryVariable(variable){

       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

/* ---------------------------------------------
* Logs user in
* --------------------------------------------- */
function login(){

    function alertDismissed() {
        // do something
    }

    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/user',
        dataType: 'json',
        data: {
            username: $('#username').val(),
            password: $('#password').val()
        },
        success: function(data){
            if(data.error === false){
                window.location.href = data.url+'?user='+data.user;
            }else if(data.error === true){
                navigator.notification.alert(
                    'The username or password you entered is incorrect. Please try again.',  // message
                    alertDismissed,         // callback
                    'Incorrect Credentials',            // title
                    'Done'                  // buttonName
                );
                console.log('error');
            }
        },
        error: function(){
            console.log('fail');
        }
    });
}

/* ---------------------------------------------
* Loads dasboard data on page load
* --------------------------------------------- */
function loadDashboard(){

    $('#settinghref').attr("href", "settings.html?user=" + getQueryVariable("user"));

    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/dashboard',
        dataType: 'json',
        success: function(data){
            $.each(data, function(i,item){

                if(item.total_orders >0 ){$('#today-amount').html(item.total_orders);}else{$('#today-amount').html("0.00");}
                $('#open-orders').html(item.open_orders);
                $('#out-stock').html(item.out_stock);
                $('#orders-today').html(item.orders_today);
            });            
        },
        error: function(){
            console.log('fail');
        }

    });

    $.ajax({
       type: 'GET',
        url: 'http://localhost/bococompany/traffic/gettodaytraffic',
        dataType: 'json',
        beforeSend: function() { $('#wait').show(); },
        success: function(data){
                    
            $.each(data, function(i,item){
                $('#today-visits').html(item.visits);
                $('#today-pageviews').html(item.pageviews);
            })


            //Close loder
            $('#loader').fadeOut();
        },
        error: function(xhr, thrownError){
            console.log('ERROR: ' + thrownError);
            console.log("status:" + xhr.status);
            console.log("threw:" + thrownError);    
        },
        complete: function() { $('#wait').hide(); }
    });  
}

/* ---------------------------------------------
* Loads single product
* --------------------------------------------- */
function loadSingleProduct(){

    var productid = getQueryVariable("productid");


    var Product = {
        init: function( ) {

            this.ajaxCall();
            return this;
        },

        ajaxCall: function() {
            var self = Product;

                $.ajax({
                    type: 'POST',
                    url: 'http://localhost/bococompany/products/getproduct',
                    dataType: 'json',
                    data: {
                        productid: productid
                    },
                    success: function(data){

                        //Pass data to buildData method
                        self.buildData(data);

                        $.each(data, function(i,item){
                            self.getExt(item.name);

                            return false; 
                        })
                        
                    },
                    error: function(){
                        console.log('fail');
                    }
                });
        },

        buildData: function(data){
            $.each(data, function(i,item){
                var list = '';
                var name = item.type;
                $('#product-list').append(list);

                $('#title').html('<strong>'+name+'</strong>');
                $('#type').html(item.type);
                $('#item-desc-popup').html('<p>'+name+ ' - ' +item.type+ '</p>');
                $('#item-desc').html(item.description);

                var list = '<li>'+item.size+' - '+item.stock_amt+' - &pound;'+item.price+'</li>';

                $('.variants').append(list);
                
                var variants =  '<div class="input-group" id="variant-'+item.detail_id+'">'+
                                    '<div class="input-row"><label>Size</label><input type="text" id="update-size-'+item.detail_id+'" value="'+item.size+'"></div>' +
                                    '<div class="input-row"><label>Stock Qty</label><input type="text" pattern="[0-9]*" id="update-stock-'+item.detail_id+'" value="'+item.stock_amt+'"></div>' +
                                    '<div class="input-row"><label>Price (&pound;)</label><input type="tel" id="update-price-'+item.detail_id+'" value="'+item.price+'"></div>' +
                                    '<div class="input-row"><label><a href="#" class="button-negative" onclick="deleteVariant('+item.detail_id+');">Delete</a></label>'+
                                    '<label><a href="#" class="button-positive" onclick="updateVairant('+item.detail_id+');">Save</a></label></div>' +
                                '</div>';

                $('#variants-edit').append(variants);

                
            });
        },

        getExt: function(product){

            //Load external details
            $.ajax({
                type: 'POST',
                url: 'http://localhost/bococompany/external_stock/etsy',
                dataType: 'json',
                data: {
                    search: product
                },
                success: function(data){

                    //Close loder
                    $('#loader').fadeOut();
                    
                    $.each(data, function(i,item){
                        var extlist = '<li>One Size - '+ item.quantity +' - &pound;'+ item.price +'</li>';

                        $('#external-variants').append(extlist);
                    });
                },
                error: function(){
                    console.log('fail');
                }
            }); 

        }

        
    };
    window.Product = Product.init();
}

/* ---------------------------------------------
* Add Variants to div
* --------------------------------------------- */
function addVariants(){
    var variants =  '<div class="input-group">'+
                    '<div class="input-row"><label>Size</label><input type="text" value="" id="add-new-size"></div>' +
                    '<div class="input-row"><label>Stock Qty</label><input type="text" value="" id="add-new-stock"></div>' +
                    '<div class="input-row"><label>Price (&pound;)</label><input type="text" value="" id="add-new-price"></div>' +
                    '<div class="input-row"><label><a href="#" class="button-positive" onclick="addNewVairant();">Save</a></label></div>' +
                    '<div>';    
    $('#variants-edit').append(variants);
    $('#add-variants').hide();    
}

/* ---------------------------------------------
* Save new product variant
* --------------------------------------------- */
function addNewVairant(){

    function alertDismissed(){

    }

        //Set variables up
    var size = $('#add-new-size').val();
        stock = $('#add-new-stock').val();
        price = $('#add-new-price').val();

    if(size == '' || stock == '' || price == ''){
        navigator.notification.alert(
            'Please enter a size, stock qty and price.',  // message
            alertDismissed,         // callback
            'Data Required',            // title
            'Done'                  // buttonName
        );
    }else{
        $.ajax({
            type: 'POST',
            url: 'http://localhost/bococompany/products/addvariance',
            dataType: 'json',
            data: {
                productid: getQueryVariable("productid"),
                size: size,
                stock: stock,
                price: price
            },
            success: function(data){
                if(data.error === false){
                    $.each(data, function(i,item){
                        var list = '<li>'+item.size+' - '+item.stock_amt+' - &pound;'+item.price+'</li>';
                        
                        $('.variants').append(list);
                    });

                    console.log('success');
                }else if(data.error === true){
                    console.log('error');
                }

                navigator.notification.alert(
                    'The new variant has been added.',  // message
                    alertDismissed,         // callback
                    'Added successfully',            // title
                    'Done'                  // buttonName
                );
            },
            error: function(){
                console.log('fail');
            }
        }); 
    }  
}

/* ---------------------------------------------
*  Update product variant
* --------------------------------------------- */
function updateVairant(detailid){

    function alertDismissed() {
        // do something
    }

    //Set variables up
    var size = $('#update-size-'+detailid).val();
        stock = $('#update-stock-'+detailid).val();
        price = $('#update-price-'+detailid).val();

    if(size == '' || stock == '' || price == ''){

        navigator.notification.alert(
            'Please enter a size, stock qty and price.',  // message
            alertDismissed,         // callback
            'Data Required',            // title
            'Done'                  // buttonName
        );

    }else{
        $.ajax({
            type: 'POST',
            url: 'http://localhost/bococompany/products/updateVairant',
            dataType: 'json',
            data: {
                productid: getQueryVariable("productid"),
                detailid: detailid,
                size: size,
                stock: stock,
                price: price
            },
            success: function(data){
                if(data.error === false){
                    
                    // $.each(data, function(i,item){
                    //     var list = '<li>'+item.size+' - '+item.stock_amt+' - &pound;'+item.price+'</li>';
                        
                    //     $('.variants').append(list);
                    //     console.log(list);
                    // });

                    console.log('success');
                }else if(data.error === true){

                    console.log('error');
                }
                navigator.notification.alert(
                    'The product has been updated with the new variants.',  // message
                    alertDismissed,         // callback
                    'Product Updated',            // title
                    'Done'                  // buttonName
                );
            },
            error: function(){
                console.log('fail');
            }
        });
    } 
}
/* ---------------------------------------------
* Save product changes
* --------------------------------------------- */
function saveProduct(){
    // $.ajax({
    //     type: 'POST',
    //     url: 'http://localhost/bococompany/products/addvariance',
    //     dataType: 'json',
    //     data: {
    //         productid: getQueryVariable("productid"),
    //         size: $('#add-size').val(),
    //         stock: $('#add-stock').val(),
    //         price: $('#add-price').val()
    //     },
    //     success: function(data){
    //         if(data.error === false){
    //             $.each(data, function(i,item){
    //                 var list = '<li>'+item.size+' - '+item.stock_amt+' - &pound;'+item.price+'</li>';
                    
    //                 $('.variants').append(list);
    //             });

    //             console.log('success');
    //         }else if(data.error === true){
    //             console.log('error');
    //         }
    //     },
    //     error: function(){
    //         console.log('fail');
    //     }
    // });   
}

/* ---------------------------------------------
* Delete product variant
* --------------------------------------------- */
function deleteVariant(variantid){

    $('#variant-'+variantid).slideUp();

    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/products/deletevariance',
        dataType: 'json',
        data: {
            detailid: variantid
        },
        success: function(data){
            if(data.error === false){
                console.log('success');
            }else if(data.error === true){
                console.log('error');
            }
        },
        error: function(){
            console.log('fail');
        }
    });
}

/* ---------------------------------------------
* Load products on page load
* --------------------------------------------- */
function loadProducts(){
    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/products',
        dataType: 'json',
        success: function(data){
            if(data.error === true){
                alert('error');
            }else{
                $.each(data, function(i,item){
                    var list =  '<li><a href="single-product.html?productid='+item.product_id+'" data-ignore="push" id="productload" >'+
                                '<h2>'+item.name+'</h2><p>'+item.type+'</p>'+
                                '<span class="chevron"></span><span class="chevron"></span></a></li>';
                    $('#product-list').append(list);
                });
            }

            //Close loder
            $('#loader').fadeOut();  
        },
        error: function(){
            console.log('error');
        }
    });
};

/* ---------------------------------------------
* Load orders on page load
* --------------------------------------------- */
function loadOrders(){

    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/orders',
        dataType: 'json',
        success: function(data){
            if(data.error === true){
                alert('error');
            }else{
                $.each(data, function(i,item){
                    if(item.fulfilled == 0){ var fulfilled = 'Not fulfilled'; }else{ var fulfilled = 'Fulfilled'; };

                    var list =  '<li><a href="single-order.html?orderid='+item.order_id+'" data-ignore="push">'+
                                '<h2>'+item.name+' '+item.surname+'</h2><p>&pound;'+item.order_amount+' - Paid &amp; '+ fulfilled +'</p>'+
                                '<span class="chevron"></span><span class="chevron"></span></a></li>';
                    $('#order-list').append(list);
                });
            }

            //Close loder
            $('#loader').fadeOut();  
        },
        error: function(){
            console.log('error');
        }
    });   
}

/* ---------------------------------------------
* Load single order details 
* --------------------------------------------- */
function loadSingleOrder(){

    //Return sold products
    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/orders/getorderdetails',
        dataType: 'json',
        data: {
            orderid: getQueryVariable("orderid")
        },
        success: function(data){
            $.each(data, function(i,item){

                var ordereditems = '<li>'+
                                    '<p>'+item.name+'</p>' +
                                    '<p>'+item.qty+' x '+ item.size +'</p>'+
                                    '<p class="right-align">&pound;'+item.price+'</p>' +
                                    '</li>';

                $('#ordered-items').append(ordereditems);
            });            
        },
        error: function(){
            console.log('fail');
        }
    });  
    
    //Return order address details
    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/orders/getorder',
        dataType: 'json',
        data: {
            orderid: getQueryVariable("orderid")
        },
        success: function(data){
            $.each(data, function(i,item){
                
                $('#name').html(item.name +' '+ item.surname);
                $('#address').html(item.street +', '+ item.city);

                if(item.discount_type != ""){ var discount = '<li><p> Discount: ' + item.discount_type +' <span> &pound;-'+ item.discount_amt +'</span></li>'}else{discount = '';}

                var ordersummary =  discount +
                                    '<li><p> Subtotal <span> &pound;'+ item.order_amount +'</span></li>'+
                                    '<li><p> Delivery: ' + item.postage_type +' <span> &pound;'+ item.postage_amt +'</span></li>'+
                                    '<li><p> Total <span> &pound;'+ item.total_amount +'</span></li>';

                $('.order-summary').append(ordersummary);

                $('#total2').html('&pound;'+ item.total_amount);

            });
                                
            //Close loder
            $('#loader').fadeOut();         
        },
        error: function(){
            console.log('fail');
        }
    });

}
/* ---------------------------------------------
* Load Settings data
* --------------------------------------------- */
function loadSettings(){
    $.ajax({
        type: 'POST',
        url: 'http://localhost/bococompany/settings',
        dataType: 'json',
        data: {
            userid: $('#uid').val()
        },
        success: function(data){
            $.each(data, function(i,item){
                
                $('.title').html(item.company);
                $('#username').html(item.username);
                $('#edit-username').val(item.username);
                $('#ganalyticsid').val(item.ganalytics_id);
            });
                                
            //Close loder
            $('#loader').fadeOut();      
        },
        error: function(){
            console.log('fail');
        }
    });  
}

/* ---------------------------------------------
* Update changed settings data
* --------------------------------------------- */
function editSettings(){

    function alertDismissed(){

    }

    //Variables
    var user = $('#edit-username').val();
        gid = $('#ganalyticsid').val();

    if(user == '' || gid == ''){
        navigator.notification.alert(
            'Please enter a Analytic ID and Email.',  // message
            alertDismissed,         // callback
            'Missing Details',            // title
            'Done'                  // buttonName
        );
    }else{
        $.ajax({
            type: 'POST',
            url: 'http://localhost/bococompany/settings/updatesettings',
            dataType: 'json',
            data: {
                userid: $('#uid').val(),
                username: user,
                ganalyticsid: gid

            },
            success: function(data){
                navigator.notification.alert(
                    'Your details have now been updated.',  // message
                    alertDismissed,         // callback
                    'Details Updated',            // title
                    'Done'                  // buttonName
                );
            },
            error: function(){
                console.log('fail');
            }
        });  
    }
}

/* ---------------------------------------------
* Load website traffic
* --------------------------------------------- */
function loadTraffic(){
    (function($) {
            
        var Graph = {
            init: function( ) {

                this.cache();
                this.ajaxCall();
                return this;
            },

            cache: function() { 
                // this.button = this.settings.button;
                // this.dataSource = this.settings.dataSource;
            },

            ajaxCall: function() {
                var self = Graph;

                $.ajax({
                   type: 'GET',
                    url: 'http://localhost/bococompany/traffic/gettrafficdata',
                    dataType: 'json',
                    data: {},
                    success: function loadGraph(data){
                        var blkstr = [];
                        $.each(data, function(i,item){

                            var weeklyhits =    '<li><strong>Visits</strong> <span>'+ item.visits +'</span></li>' + 
                                                '<li><strong>Page Views</strong> <span>'+ item.pageviews +'</span> </li>'+
                                                '<li><strong>Average Time on Site (seconds)</strong> <span>'+ item.avg_site +'</span> </li>'+
                                                '<li><strong>Bounce Rate</strong> <span>'+ item.bounce_rate +'%</span> </li>';

                            $('#weekly-hits').append(weeklyhits);

                            var str = item.visits;
                            blkstr.push(str);
                        })
                        var test = blkstr.join(",");
                        self.buildGraph(test);
                    },
                    error: function(xhr, thrownError){
                        console.log('ERROR: ' + thrownError);
                        console.log("status:" + xhr.status);
                        console.log("threw:" + thrownError);    
                    }
                }); 
            },

            renderData: function(data){
                $.each(data, function(i,item){
                    console.log(data);
                })
            },

            buildGraph: function(visit){

                // var graphData = visit;
                // console.log(visit);

                var graphData = [334,171,216,180,57,47,122,80]

                var lineChartData = {
                        labels : ["","","","","","","",""],
                        datasets : [
                            {
                                fillColor : "rgba(151,187,205,0.5)",
                                strokeColor : "rgba(151,187,205,1)",
                                pointColor : "rgba(151,187,205,1)",
                                pointStrokeColor : "#fff",
                                data : graphData
                            }
                        ]

                    }

                var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);    

                //Close loder
                
                $('#loader').fadeOut();

            }

        };

        window.Graph = Graph.init();



    })(jQuery);

}